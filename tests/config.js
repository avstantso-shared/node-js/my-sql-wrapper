const path = require('path');
const dotenv = require('dotenv');
dotenv.config({ path: path.resolve(__dirname, '../.env') });

const Promise = require('bluebird');

const createDemoDB = require('../utils/internal/createDemoDB');
const generate = require('../utils/./internal/generate');

let callCounter = 0;
module.exports = async () => {
  if (callCounter) return;
  callCounter++;

  await Promise.mapSeries([createDemoDB, generate], async (cb) =>
    cb({ silent: true })
  );
};
