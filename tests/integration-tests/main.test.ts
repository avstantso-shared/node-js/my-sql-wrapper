import 'jest';
import mysql, { Pool } from 'mysql2/promise';
import Bluebird from 'bluebird';
import {
  MySqlQueryError,
  Executor,
  As,
  Value,
  Conditions,
  Desc,
  ColDefConverter,
} from '@avstantso/node-js--my-sql-wrapper';
// "../../generated/avstantso_demo" files will be generated after run:
//   yarn my-sql-wrapper createDemoDB && yarn my-sql-wrapper generate
//   or
//   yarn my-sql-wrapper test
import {
  schemaName as database,
  Procedures,
  Functions,
  Views,
} from '../generated/avstantso_demo';

const { SignalIf, SpellSelect } = Procedures;
const {
  UPPER,
  BIN_TO_UUID,
  UUID_TO_BIN,
  IsDarkSpell,
  IsLightSpell,
  SpellById,
  SpellId,
} = Functions;
const { vw_spells } = Views;
const { Eq, And, Like, IsNotNull } = Conditions;

describe('my-sql-wrapper', () => {
  let mysqlPool: Pool;
  let executor: Executor;

  beforeAll(async () => {
    mysqlPool = await mysql.createPool({
      connectionLimit: 10,
      Promise: Bluebird,
      host: process.env.DB_HOST || 'localhost',
      port: Number.parseInt(process.env.DB_PORT, 10) || 3306,
      user: process.env.DB_USER || 'root',
      password: process.env.DB_PASSWORD,
      database,
    });

    executor = Executor({ pool: mysqlPool });
  });

  afterAll(async () => {
    executor = undefined;
    await mysqlPool.end();
  });

  it('is-bluebird-promises', async () => {
    const { select, selectView, call } = executor;

    const pSelect = select(1);
    expect((pSelect as any).tap).toBeTruthy();
    await pSelect;

    const pSelectView = selectView(vw_spells, ({ id }) => [id]);
    expect((pSelectView as any).tap).toBeTruthy();
    const id = await pSelectView.then(([rows]) => rows[0].id);

    const pCall = call(SpellSelect(id)).then(
      ColDefConverter(({ name, columnType }, data) => {
        if (1 === columnType && ['system'].includes(name)) return !!data;
        return data;
      })
    );
    expect((pCall as any).tap).toBeTruthy();
    await pCall;
  });

  it('select-simple', async () => {
    const { select } = executor;

    await select(
      As(UPPER(Value('Fly')), 'up'),
      As(IsLightSpell('sun_ray'), 'sr')
    ).then(([rows]) => {
      expect(rows.length).toBe(1);

      const { up, sr } = rows[0];

      expect(up).toBe('FLY');
      expect(sr).toBe(true);
    });
  });

  describe('select-view', () => {
    it('simple', async () => {
      const { selectView, select } = executor;

      const { id: spell_id } = await selectView(
        vw_spells,
        ({ id, name, description }) => [
          As(BIN_TO_UUID(id, 1), 'id'),
          name,
          description,
          IsDarkSpell(name),
        ]
      ).then(([rows]) => {
        expect(rows.length).toBeGreaterThanOrEqual(2);
        return rows[0];
      });

      await select(As(SpellById(UUID_TO_BIN(spell_id, 1)), 'sbi')).then(
        ([rows]) => {
          expect(rows.length).toBe(1);

          const { sbi } = rows[0];

          expect(sbi).toBe('sun_ray');
        }
      );
    });

    it('where', async () => {
      const { selectView } = executor;

      await selectView(
        vw_spells,
        ({ id, name, active, putdate, description, magic_name }) => {
          return {
            columns: [
              As(BIN_TO_UUID(id, 1), 'id'),
              As(Value('dummy'), 'd'),
              name,
              active,
              putdate,
              description,
            ],
            where: And(IsNotNull(description), Eq(magic_name, Value('light'))),
          };
        }
      ).then(([rows]) => {
        expect(rows.length).toBeGreaterThanOrEqual(1);
        const { id, d, name, active, putdate } = rows[0];

        expect(id).toBeTruthy();
        expect(d).toBe('dummy');
        expect(typeof active).toBe('boolean');
        expect(putdate instanceof Date).toBeTruthy();
        expect(name).toBe('sun_ray');
      });

      await selectView(vw_spells, ({ name }) => {
        return {
          columns: [name],
          where: Like(name, '%ur%'),
        };
      }).then(([rows]) => {
        expect(rows.length).toBe(1);
        const { name } = rows[0];

        expect(name).toBe('curse');
      });
    });

    it('orderBy', async () => {
      const { selectView } = executor;

      await selectView(vw_spells, ({ name, description }) => {
        return {
          columns: [name, description],
          orderBy: [name],
        };
      }).then(([rows]) => {
        expect(rows.length).toBeGreaterThanOrEqual(2);
        const { name: name0 } = rows[0];
        const { name: name1 } = rows[1];

        expect(name0).toBe('curse');
        expect(name1).toBe('sun_ray');
      });
    });

    it('orderBy-desc', async () => {
      const { selectView } = executor;

      await selectView(vw_spells, ({ name, description }) => {
        return {
          columns: [name, description],
          orderBy: [Desc(name)],
        };
      }).then(([rows]) => {
        expect(rows.length).toBeGreaterThanOrEqual(2);
        const { name: name0 } = rows[0];
        const { name: name1 } = rows[1];

        expect(name0).toBe('sun_ray');
        expect(name1).toBe('curse');
      });
    });
  });

  describe('call', () => {
    it('executable', async () => {
      const { call } = executor;

      const [s0, s1] = [
        { name: 'reanimate', magic: 'dark' },
        { name: 'prizmatic light', magic: 'light' },
      ];

      const [e0, e1] = await Bluebird.Promise.mapSeries(
        [s0, s1],
        async (item) =>
          call(
            SignalIf(`"${item.name}" is not dark`, 'dark' !== item.magic)
          ).then(
            () => null,
            (e) => e
          )
      );

      expect(e0).toBeFalsy();

      expect(MySqlQueryError.is(e1)).toBeTruthy();
      expect(e1.message).toBe(`"${s1.name}" is not dark`);
    });

    it('selectable', async () => {
      const { select, call } = executor;

      const { id } = await select(As(SpellId('curse'), 'id')).then(
        ([rows]) => rows[0]
      );

      const [spell] = await call(SpellSelect(id))
        .then(
          ColDefConverter(({ name, columnType }, data) => {
            if (
              1 === columnType &&
              ['system', 'active', 'confirmed', 'isDark', 'isLight'].includes(
                name
              )
            )
              return !!data;
            return data;
          })
        )
        .then((result) => {
          const [rows] = result[0] as any;
          return rows;
        });

      const { putdate, name, active, isDark, isLight } = spell;
      expect(putdate instanceof Date).toBeTruthy();
      expect({ name, active, isDark, isLight }).toStrictEqual({
        name: 'curse',
        active: true,
        isDark: true,
        isLight: false,
      });
    });
  });
});
