import { Builder, ViewFields } from '../types';
import { Condition,   Conditions } from '../classes';

const {And} = Conditions;

export function injectCondition<TViewFieds extends ViewFields>(
  fields: TViewFieds,
  callback: Builder.SelectView.Callback<TViewFieds>,
  condition: Condition
): Builder.SelectView.Callback.Result {
  const colsOrStruct = callback(fields);
  const {
    columns,
    where: initialWhere,
    ...rest
  }: Builder.SelectView.Callback.Result = Array.isArray(colsOrStruct)
    ? { columns: colsOrStruct }
    : colsOrStruct;

  const where = initialWhere ? And(condition, initialWhere) : condition;

  return { columns, where, ...rest };
}
