import { RowDataPacket } from 'mysql2';

import { Generics } from '@avstantso/node-or-browser-js--utils';

import { Func, As } from '../classes';
import { Executor } from '../types';

export namespace SelectFunc {
  export type RDP<TResult extends Func.ReturnType> = RowDataPacket & {
    r: TResult;
  };
}

export function SelectFunc(selectOrExecutor: Executor.Select | Executor) {
  const select: Executor.Select =
    Generics.Cast(selectOrExecutor).select || Generics.Cast(selectOrExecutor);

  return <TResult extends Func.ReturnType>(func: Func<TResult>) =>
    select<SelectFunc.RDP<TResult>[]>(new As(func, 'r')).then(([[{ r }]]) => r);
}
