import { Field } from '../classes';
import { toBool, toDate } from '../converters';

// AVStantso:
// Types without size also must be functions for different result on every field
export const TINYINT = () => new Field<number>();
export const INT = () => new Field<number>();
export const BIGINT = () => new Field<number>();
export const BOOL = () => new Field<boolean>(toBool);
export const DECIMAL = () => new Field<number>();
export const CHAR = (size: number) => new Field<string>(size);
export const VARCHAR = (size: number) => new Field<string>(size);
export const TEXT = () => new Field<string>();
export const MEDIUMTEXT = () => new Field<string>();
export const LONGTEXT = () => new Field<string>();
export const JSON = () => new Field<string>();
export const BINARY = (size: number) => new Field<Buffer>(size);
export const VARBINARY = (size: number) => new Field<Buffer>(size);
export const TIMESTAMP = (fsp?: number) => new Field<Date>(toDate);
export const DATETIME = (fsp?: number) => new Field<Date>(toDate);
export const DATE = () => new Field<Date>(toDate);
