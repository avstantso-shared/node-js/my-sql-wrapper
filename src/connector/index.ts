import mysql, { Connection, PoolConnection } from 'mysql2/promise';
import Bluebird from 'bluebird';

import { Perform } from '@avstantso/node-or-browser-js--utils';

import * as Types from '../types';
import { MySqlQueryError } from '../classes';
import { logDebug } from '../utils';

export namespace Connector {
  export type Options = Types.Connector.Options;

  export type Connect = Types.Connector.Connect;
  export type Release = Types.Connector.Release;
  export type Execute = Types.Connector.Execute;
}

export type Connector = Types.Connector;

export function Connector(options: Connector.Options): Connector {
  const pool = Types.Providers.is.pool(options) && options.pool;
  const connection =
    Types.Providers.is.connection(options) && options.connection;
  const { database } = options;

  let innerConnection: PoolConnection;

  const connect: Connector.Connect = (): Promise<Connection> =>
    Bluebird.resolve<Connection>(
      pool
        ? pool.getConnection().then((conn) => {
            innerConnection = conn;
            return conn as Connection;
          })
        : connection
    ).then((conn) => {
      if (database)
        return Bluebird.resolve(`USE ${'`'}${database}${'`'};`).then((sql) =>
          conn
            .query(sql)
            .then(() => conn, MySqlQueryError.reThrow(sql, undefined))
        );

      return conn;
    });

  const release: Connector.Release = (): void =>
    innerConnection && innerConnection.release();

  const execute: Connector.Execute = <
    T extends
      | mysql.RowDataPacket[]
      | mysql.RowDataPacket[][]
      | mysql.OkPacket
      | mysql.OkPacket[]
      | mysql.ResultSetHeader
  >(
    query: Perform.Promise<mysql.QueryOptions>
  ): Promise<[T, mysql.FieldPacket[]]> =>
    Bluebird.resolve(Perform.Promise(query)).then(({ sql, values }) => {
      if (values?.length) logDebug(`sql:\r\n${sql}\r\nvalues:%O`, values);
      else logDebug(`sql:\r\n${sql}`);

      return connect()
        .then((conn) => conn.query<T>(sql, values))
        .catch(MySqlQueryError.reThrow(sql, values))
        .finally(release);
    });

  return { connect, release, execute };
}
