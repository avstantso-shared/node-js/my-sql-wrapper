import type * as Types from '../types';
import { colDefConverter } from './converter';

export namespace ColDefConverter {
  /**
   * @summary ColDefConv.Context fields: from ColumnDefinition of CALL select stored procedure
   */
  export type Context = Types.ColDefConverter.Context;

  export type Rule = Types.ColDefConverter.Rule;

  export type Result<T> = Types.ColDefConverter.Result<T>;
}

export const ColDefConverter = colDefConverter;
