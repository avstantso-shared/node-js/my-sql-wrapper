import mysql from 'mysql2/promise';
import Bluebird from 'bluebird';

import type { ColDefConverter } from '../types';
import { ColDefConvStructError } from '../classes';
import { Generics } from '@avstantso/node-or-browser-js--utils';

export function colDefConverter<
  T extends
    | mysql.RowDataPacket[]
    | mysql.RowDataPacket[][]
    | mysql.OkPacket
    | mysql.OkPacket[]
    | mysql.ResultSetHeader
>(
  rules?: ColDefConverter.Rule | ColDefConverter.Rule[]
): ColDefConverter.Result<T> {
  if (!rules || (Array.isArray(rules) && !rules.length))
    return (
      results: [T, mysql.FieldPacket[]]
    ): Promise<[T, mysql.FieldPacket[]]> =>
      Bluebird.resolve<[T, mysql.FieldPacket[]]>(results);

  if (!Array.isArray(rules)) return colDefConverter([rules]);

  return (
    results: [T, mysql.FieldPacket[]]
  ): Promise<[T, mysql.FieldPacket[]]> => {
    if (!results.length) throw new ColDefConvStructError('results is empty');

    const [rowsAndRsh, columnsAndOther] = results;

    if (!Array.isArray(rowsAndRsh))
      throw new ColDefConvStructError('rowsAndRsh is not array');

    if (!Array.isArray(columnsAndOther))
      throw new ColDefConvStructError(
        `columnsAndOther is not array (${typeof columnsAndOther})`
      );

    const [rawRows, rsh] = rowsAndRsh;
    if (!Array.isArray(rawRows))
      throw new ColDefConvStructError(
        `rawRows is not array (${typeof rawRows})`
      );

    const [columns] = columnsAndOther;
    if (!Array.isArray(columns))
      throw new ColDefConvStructError(
        `columns is not array (${typeof columns})`
      );

    const rows: any = rawRows.map((row) => {
      const r = {} as mysql.RowDataPacket;
      Object.keys(row).forEach((name) => {
        const column = columns.find(({ name: cn }) => cn === name);
        if (!column)
          throw new ColDefConvStructError(`column "${name}" is not found`);

        let d = row[name];
        rules.forEach((rule) => (d = rule(column, d)));
        r[name] = d;
      });
      return r;
    });

    return Bluebird.resolve<[T, mysql.FieldPacket[]]>([
      Generics.Cast.To([rows, rsh]),
      columnsAndOther,
    ]);
  };
}
