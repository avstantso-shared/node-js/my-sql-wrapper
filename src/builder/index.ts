import type { ViewFields } from '../types';
import type * as Types from '../types/builder';

import { Call } from './call';
import { Select } from './select';
import { SelectView } from './select-view';

export namespace Builder {
  export type Options = Types.Builder.Options;

  export type Call = Types.Builder.Call;

  export type Select = Types.Builder.Select;

  export namespace SelectView {
    export namespace Callback {
      export type Result = Types.Builder.SelectView.Callback.Result;
    }

    export type Callback<TViewFields extends ViewFields> =
      Types.Builder.SelectView.Callback<TViewFields>;
  }

  export type SelectView = Types.Builder.SelectView;
}

export type Builder = Types.Builder;

export function Builder(options?: Builder.Options): Builder {
  const call = Call();
  const select = Select();
  const selectView = SelectView();

  return { call, select, selectView };
}
