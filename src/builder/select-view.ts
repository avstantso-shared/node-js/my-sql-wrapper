import { JS } from '@avstantso/node-or-browser-js--utils';

import type { ViewFields, Builder, Query, Arg } from '../types';
import { Named, Parsable, View, Desc, Distinct } from '../classes';

import * as Validate from '../validate';

export function SelectView(): Builder.SelectView {
  function doSelectView<TViewFields extends ViewFields>(
    view: View<TViewFields>,
    callback: Builder.SelectView.Callback<TViewFields>,
    distinct?: boolean
  ): [Query.Fragment, Arg[]] {
    const { fields } = view;
    const setup = callback(fields);

    const {
      columns,
      where,
      groupBy,
      orderBy,
      limit,
      offset,
    }: Builder.SelectView.Callback.Result = Array.isArray(setup)
      ? { columns: setup }
      : setup;

    columns.forEach((c) => Validate.argument(c, { allowField: true }));

    const c = Parsable.parseValues(columns);

    const w = where && where.parse();

    const gb =
      groupBy?.length &&
      groupBy.map((column) => {
        if (column instanceof Named) return column.name;
        return column;
      });

    const ob =
      orderBy?.length &&
      orderBy.map((column) => {
        if (column instanceof Named) return column.name;
        if (column instanceof Desc) return column.parse().sql;
        return column;
      });

    const q: Query.Fragment = {
      sql: [
        `SELECT${distinct ? ` ${Distinct.prefix}` : ''} ${c.sqls.join(', ')}`,
        `FROM ${view.name}`,
        w && `WHERE ${w.sql}`,
        gb && `GROUP BY ${gb.join(', ')}`,
        ob && `ORDER BY ${ob.join(', ')}`,
        JS.is.number(limit) && `LIMIT ${limit}`,
        JS.is.number(offset) && `OFFSET ${offset}`,
      ]
        .pack()
        .join('\r\n'),
      ...(c.values || w?.values
        ? { values: [...(c.values || []), ...(w?.values || [])] }
        : {}),
    };

    return [q, columns];
  }

  const selectView: Builder.SelectView = <TViewFields extends ViewFields>(
    view: View<TViewFields>,
    callback: Builder.SelectView.Callback<TViewFields>
  ): [Query.Fragment, Arg[]] => doSelectView<TViewFields>(view, callback);

  selectView.distinct = <TViewFields extends ViewFields>(
    view: View<TViewFields>,
    callback: Builder.SelectView.Callback<TViewFields>
  ): [Query.Fragment, Arg[]] => doSelectView<TViewFields>(view, callback, true);

  return selectView;
}
