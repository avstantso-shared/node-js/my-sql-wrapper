import { InvalidArgumentError } from '@avstantso/node-or-browser-js--errors';

import type { Builder } from '../types';
import { Parsable } from '../classes';

export function Select(): Builder.Select {
  return (...params: any[]) => {
    if (!params?.length)
      throw new InvalidArgumentError(
        `Builder.select params length must be >= 1`
      );

    const { sqls, values } = Parsable.parseValues(params);
    const q = {
      sql: `SELECT ${sqls.join(', ')}`,
      ...(values?.length ? { values } : {}),
    };

    return q;
  };
}
