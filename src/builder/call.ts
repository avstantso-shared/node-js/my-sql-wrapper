import type { Builder } from '../types';

export function Call(): Builder.Call {
  return (proc) => proc.parse();
}
