import mysql from 'mysql2/promise';

import type { Executor } from '../types';
import { Proc } from '../classes';

import { Connector } from '../connector';
import { Builder } from '../builder';

export function Call(builder: Builder, { execute }: Connector): Executor.Call {
  const call: Executor.Call = <
    T extends
      | mysql.RowDataPacket[]
      | mysql.RowDataPacket[][]
      | mysql.OkPacket
      | mysql.OkPacket[]
      | mysql.ResultSetHeader
  >(
    proc: Proc,
    options?: Executor.Exec.Options
  ): Promise<[T, mysql.FieldPacket[]]> => {
    const { beforeExec } = options || {};

    const promise = execute<T>(() => {
      const q = builder.call(proc);
      beforeExec && beforeExec(q);
      return q;
    });

    if (!proc.onrejected) return promise;

    return promise.catch(proc.onrejected);
  };

  return call;
}
