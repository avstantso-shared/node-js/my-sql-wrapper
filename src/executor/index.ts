import type * as Types from '../types/executor';

import { Builder } from '../builder';
import { Connector } from '../connector';

import { Call } from './call';
import { Select } from './select';
import { SelectView } from './select-view';
import { Execute } from './execute';

export namespace Executor {
  export type Options = Types.Executor.Options;

  export namespace Exec {
    export namespace Options {
      export type Hook = Types.Executor.Exec.Options.Hook;
    }

    export type Options = Types.Executor.Exec.Options;
  }

  export type Call = Types.Executor.Call;
  export type Select = Types.Executor.Select;
  export type SelectView = Types.Executor.SelectView;
}

export type Executor = Types.Executor;

export function Executor(options: Executor.Options): Executor {
  const builder = Builder(options);
  const conn = Connector(options);

  const call = Call(builder, conn);
  const select = Select(builder, conn);
  const selectView = SelectView(builder, conn);
  const execute = Execute(conn);

  return { call, select, selectView, execute };
}
