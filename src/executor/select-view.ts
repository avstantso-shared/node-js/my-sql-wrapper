import mysql from 'mysql2/promise';
import Bluebird from 'bluebird';

import type { ViewFields, Executor } from '../types';
import { View } from '../classes';
import { Connector } from '../connector';
import { Builder } from '../builder';

import { convertRows } from './convert-rows';

export function SelectView(
  builder: Builder,
  { execute }: Connector
): Executor.SelectView {
  function doSelectView<
    T extends mysql.RowDataPacket[],
    TViewFields extends ViewFields
  >(
    view: View<TViewFields>,
    callback: Builder.SelectView.Callback<TViewFields>,
    options: Executor.Exec.Options,
    distinct?: boolean
  ): Promise<[T, mysql.FieldPacket[]]> {
    return Bluebird.resolve().then(() => {
      const [q, columns] = (
        distinct ? builder.selectView.distinct : builder.selectView
      )(view, callback);

      const { beforeExec } = options || {};

      beforeExec && beforeExec(q);

      return execute<T>(q).then(convertRows<T>(columns));
    });
  }

  const selectView: Executor.SelectView = <
    T extends mysql.RowDataPacket[],
    TViewFields extends ViewFields
  >(
    view: View<TViewFields>,
    callback: Builder.SelectView.Callback<TViewFields>,
    options?: Executor.Exec.Options
  ): Promise<[T, mysql.FieldPacket[]]> =>
    doSelectView<T, TViewFields>(view, callback, options);

  selectView.distinct = <
    T extends mysql.RowDataPacket[],
    TViewFields extends ViewFields
  >(
    view: View<TViewFields>,
    callback: Builder.SelectView.Callback<TViewFields>,
    options?: Executor.Exec.Options
  ): Promise<[T, mysql.FieldPacket[]]> =>
    doSelectView<T, TViewFields>(view, callback, options, true);

  return selectView;
}
