import mysql from 'mysql2/promise';

import type { Executor, Query } from '../types';

import { Connector } from '../connector';

export function Execute(conn: Connector): Executor.Execute {
  const execute: Executor.Execute = <
    T extends
      | mysql.RowDataPacket[]
      | mysql.RowDataPacket[][]
      | mysql.OkPacket
      | mysql.OkPacket[]
      | mysql.ResultSetHeader
  >(
    ...fragments: Query.Fragment[]
  ): Promise<[T, mysql.FieldPacket[]]> => {
    const sqls: string[] = [];
    const values: any[] = [];

    fragments.forEach((fr) => {
      sqls.push(fr.sql);
      if (fr.values?.length) values.push(...fr.values);
    });

    const q = {
      sql: sqls.join(';\r\n'),
      ...(values?.length ? { values } : {}),
    };

    return conn.execute<T>(q);
  };

  return execute;
}
