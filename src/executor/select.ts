import mysql from 'mysql2/promise';

import { InvalidArgumentError } from '@avstantso/node-or-browser-js--errors';

import type { Executor } from '../types';
import { Parsable } from '../classes';

import * as Validate from '../validate';
import { Connector } from '../connector';
import { Builder } from '../builder';

import { convertRows } from './convert-rows';

export function Select(
  builder: Builder,
  { execute }: Connector
): Executor.Select {
  const select: Executor.Select = <T extends mysql.RowDataPacket[]>(
    ...params: any[]
  ) => {
    if (!params?.length)
      throw new InvalidArgumentError(
        `Executor.select params length must be >= 1`
      );

    let options: Executor.Exec.Options;
    for (let i = 0; i < params.length; i++) {
      const isOptions = params[i].beforeExec;

      if (!isOptions) Validate.argument(params[i]);
      else if (i < params.length - 1)
        throw new InvalidArgumentError(
          `Executor.select options must be a last param`
        );
      else options = params.pop();
    }
    const { beforeExec } = options || {};

    const [param0, ...rest] = params;

    return execute<T>(() => {
      const q = builder.select(param0, ...rest);
      beforeExec && beforeExec(q);
      return q;
    }).then(convertRows<T>(params));
  };

  return select;
}
