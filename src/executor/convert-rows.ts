import mysql from 'mysql2/promise';
import Bluebird from 'bluebird';

import { NamedConv } from '../classes';

import type { Convertable } from '../types';

export function convertRows<T extends mysql.RowDataPacket[]>(columns: any[]) {
  const converters = columns.reduce<NamedConv[]>((r, c) => {
    if (c instanceof NamedConv && c.convert) r.push(c);
    return r;
  }, []);

  return (rowsAndFields: [T, mysql.FieldPacket[]]) => {
    if (!converters.length) return rowsAndFields;

    const [rawRows, fields] = rowsAndFields;

    const rows: any = rawRows.map((row) => {
      const r = {} as mysql.RowDataPacket;
      Object.keys(row).forEach((name) => {
        const { convert: c } =
          converters.find(({ name: cn }) => cn === name) || ({} as Convertable);
        r[name] = c ? c(row[name]) : row[name];
      });
      return r;
    });

    return Bluebird.resolve<[T, mysql.FieldPacket[]]>([rows, fields]);
  };
}
