import type { GroupFunctions as Types } from '../types';
import { Count as _Count } from './count';

export namespace GroupFunctions {
  export namespace Count {
    export type Expression = Types.Count.Expression;
  }

  export type Count = Types.Count;
}

export type GroupFunctions = Types;

export const GroupFunctions = {
  Count: _Count,
};
