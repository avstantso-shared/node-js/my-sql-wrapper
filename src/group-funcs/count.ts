import { Generics } from '@avstantso/node-or-browser-js--utils';

import type { GroupFunctions } from '../types';
import { Func, Distinct } from '../classes';

export const Count: GroupFunctions.Count = (expression) =>
  Func.raw(Count.name, Generics.Cast.To(expression));

Count.distinct = (expression) => Count(new Distinct(expression));
