import type { Convert } from '../types';

export const toBool: Convert<boolean, number> = (a) => !!a;
export const toDate: Convert<Date, string> = (a) => new Date(a);
