export type Convert<TTo = any, TFrom = any> = (aFrom: TFrom) => TTo;

export interface Convertable {
  convert: Convert;
}
