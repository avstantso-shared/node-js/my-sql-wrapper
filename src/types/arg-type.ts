export type ArgType =
  | string
  | number
  | bigint
  | boolean
  | symbol
  | undefined
  | null
  | Date
  | Buffer;
