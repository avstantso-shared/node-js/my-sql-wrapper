import type mysql from 'mysql2/promise';

export namespace ColDefConverter {
  /**
   * @summary ColDefConv.Context fields: from ColumnDefinition of CALL select stored procedure
   */
  export interface Context {
    name: string;
    columnType: number;
    flags: number;
    decimals: number;
  }

  export type Rule = (context: Context, data: any) => any;

  export type Result<T> = (
    results: [T, mysql.FieldPacket[]]
  ) => Promise<[T, mysql.FieldPacket[]]>;
}
