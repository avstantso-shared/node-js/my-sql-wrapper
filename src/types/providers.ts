import type mysql from 'mysql2/promise';

import { JS } from '@avstantso/node-or-browser-js--utils';

import type * as Ex from './executor';

export namespace Providers {
  export interface Pool {
    pool: mysql.Pool;
  }

  export interface Connection {
    connection: mysql.Connection;
  }

  export interface Executor {
    executor: Ex.Executor;
  }

  export type PoolOrConn = Pool | Connection;
}

function isPool(candidate: Providers.PoolOrConn): candidate is Providers.Pool {
  return null !== candidate && JS.is.object(candidate) && 'pool' in candidate;
}

function isConnection(
  candidate: Providers.PoolOrConn
): candidate is Providers.Connection {
  return (
    null !== candidate && JS.is.object(candidate) && 'connection' in candidate
  );
}

export const Providers = {
  is: {
    pool: isPool,
    connection: isConnection,
  },
};
