export namespace Query {
  export namespace Fragment {
    export interface List {
      sqls: string[];
      values?: any[];
    }
  }

  export interface Fragment {
    sql: string;
    values?: any[];
  }
}
