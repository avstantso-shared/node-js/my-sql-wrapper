import type { ArgType } from './arg-type';
import type { ViewFields } from './viewFields';
import type { Func, Proc, View } from '../classes';

export namespace Definition {
  export interface Functions {
    [key: string]: <TResult extends ArgType, TParams extends any[]>(
      ...params: TParams
    ) => Func<TResult>;
  }

  export interface Procedures {
    [key: string]: <TParams extends any[]>(...params: TParams) => Proc;
  }

  export interface Views {
    [key: string]: View<ViewFields>;
  }
}

export interface Definition<
  TSysFunctions extends Definition.Functions,
  TFunctions extends Definition.Functions,
  TProcedures extends Definition.Procedures,
  TViews extends Definition.Views
> {
  schemaName: string;
  schemaNameEsc: string;
  sysFunctions?: TSysFunctions;
  Functions: TFunctions;
  Frocedures: TProcedures;
  Views: TViews;
}
