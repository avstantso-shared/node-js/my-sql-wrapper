import { Func } from '../classes/func/abstract';
import { Value } from '../classes/value';
import { Field } from '../classes/field';
import { As } from '../classes/as';

import { ArgType } from './arg-type';

export namespace Arg {
  export type Type = ArgType;
}

export type Arg<T extends ArgType = ArgType> =
  | T
  | Func<T>
  | Func
  | Value<T>
  | Value
  | Field<T>
  | Field
  | As<T>
  | As
  | undefined
  | null;
