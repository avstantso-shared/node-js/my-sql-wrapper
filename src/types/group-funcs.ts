import { Asterisk, Func, Field, Value, Distinct } from '../classes';

export namespace GroupFunctions {
  export namespace Count {
    export type Expression = Asterisk | Field | Func | Value;
  }

  export type Count = {
    (expression: Count.Expression | Distinct): Func;
    distinct(expression: Count.Expression): Func;
  };
}

export type GroupFunctions = {
  Count: GroupFunctions.Count;
};
