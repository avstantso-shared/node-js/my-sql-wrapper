import type { ArgType } from './arg-type';
import type { Field } from '../classes/field';

export interface ViewFields {
  [key: string]: Field<ArgType>;
}
