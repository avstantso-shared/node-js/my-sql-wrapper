import mysql, { Connection, ConnectionOptions } from 'mysql2/promise';

import type { Perform } from '@avstantso/node-or-browser-js--utils';

import type { Providers } from './providers';

export namespace Connector {
  export type Options = Providers.PoolOrConn &
    Pick<ConnectionOptions, 'database'>;

  export type Connect = () => Promise<Connection>;
  export type Release = () => void;
  export type Execute = <
    T extends
      | mysql.RowDataPacket[]
      | mysql.RowDataPacket[][]
      | mysql.OkPacket
      | mysql.OkPacket[]
      | mysql.ResultSetHeader
  >(
    query: Perform.Promise<mysql.QueryOptions>
  ) => Promise<[T, mysql.FieldPacket[]]>;
}

export interface Connector {
  connect: Connector.Connect;
  release: Connector.Release;
  execute: Connector.Execute;
}
