import type mysql from 'mysql2/promise';
import type { Proc, View } from '../classes';
import type { Arg } from './arg';
import type { Connector } from './connector';
import type { Query } from './parser';
import type { ViewFields } from './viewFields';
import type { Builder } from './builder';

export namespace Executor {
  export type Options = Connector.Options & Builder.Options;

  export namespace Exec {
    export namespace Options {
      export type Hook = (query: Query.Fragment) => void;
    }

    export type Options = {
      beforeExec: Options.Hook;
    };
  }

  export type Call = <
    T extends
      | mysql.RowDataPacket[]
      | mysql.RowDataPacket[][]
      | mysql.OkPacket
      | mysql.OkPacket[]
      | mysql.ResultSetHeader
  >(
    proc: Proc,
    options?: Exec.Options
  ) => Promise<[T, mysql.FieldPacket[]]>;

  export type Select = <T extends mysql.RowDataPacket[]>(
    param: Arg,
    ...params: [...Arg[], Exec.Options] | Arg[]
  ) => Promise<[T, mysql.FieldPacket[]]>;

  export namespace SelectView {
    export type Signature = <
      T extends mysql.RowDataPacket[],
      TViewFields extends ViewFields
    >(
      view: View<TViewFields>,
      callback: Builder.SelectView.Callback<TViewFields>,
      options?: Exec.Options
    ) => Promise<[T, mysql.FieldPacket[]]>;
  }

  export type SelectView = SelectView.Signature & {
    distinct: SelectView.Signature;
  };

  export type Execute = <
    T extends
      | mysql.RowDataPacket[]
      | mysql.RowDataPacket[][]
      | mysql.OkPacket
      | mysql.OkPacket[]
      | mysql.ResultSetHeader
  >(
    ...fragments: Query.Fragment[]
  ) => Promise<[T, mysql.FieldPacket[]]>;
}

export interface Executor {
  call: Executor.Call;
  select: Executor.Select;
  selectView: Executor.SelectView;

  execute: Executor.Execute;
}
