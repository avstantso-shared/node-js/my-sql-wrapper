import type { Named, Proc, View, Condition, Desc } from '../classes';
import type { Arg } from './arg';
import type { Query } from './parser';
import type { ViewFields } from './viewFields';

export namespace Builder {
  export type Options = {};

  export type Call = (proc: Proc) => Query.Fragment;

  export type Select = (param: Arg, ...params: Arg[]) => Query.Fragment;

  export namespace SelectView {
    export namespace Callback {
      export interface Result {
        columns: Arg[];
        where?: Condition;
        groupBy?: (Named | string)[];
        orderBy?: (Named | Desc | string)[];
        limit?: number;
        offset?: number;
      }
    }

    export type Callback<TViewFields extends ViewFields> = (
      fields: TViewFields
    ) => Callback.Result | Arg[];

    export type Signature = <TViewFields extends ViewFields>(
      view: View<TViewFields>,
      callback: SelectView.Callback<TViewFields>
    ) => [Query.Fragment, Arg[]];
  }

  export type SelectView = SelectView.Signature & {
    distinct: SelectView.Signature;
  };
}

export interface Builder {
  call: Builder.Call;
  select: Builder.Select;
  selectView: Builder.SelectView;
}
