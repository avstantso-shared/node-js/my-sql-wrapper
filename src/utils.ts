import debug from 'debug';

export const logDebug = debug('mswexec');

export function escape(name: string) {
  return '`' + name + '`';
}
