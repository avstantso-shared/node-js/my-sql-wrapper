import { BaseError } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';

export class MySqlWrapperArgError extends BaseError {
  index: number;

  constructor(
    current: string,
    nested: string,
    index?: number,
    internal?: Error
  ) {
    super(
      `"${nested}"${
        JS.is.number(index) ? ` #${index}` : ''
      } nesting attempt in "${current}"`,
      internal
    );

    this.index = index;

    Object.setPrototypeOf(this, MySqlWrapperArgError.prototype);
  }

  static is(error: unknown): error is MySqlWrapperArgError {
    return JS.is.error(this, error);
  }

  static for<T extends object>(
    objClass: new (...args: any) => T,
    nested: string,
    index?: number
  ) {
    return new MySqlWrapperArgError(objClass.name, nested, index);
  }
}
