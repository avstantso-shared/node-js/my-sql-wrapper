import { BaseError } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';

export class ColDefConverterStructError extends BaseError {
  constructor(message: string, internal?: Error) {
    super(message, internal);

    Object.setPrototypeOf(this, ColDefConverterStructError.prototype);
  }

  static is(error: unknown): error is ColDefConverterStructError {
    return JS.is.error(this, error);
  }
}
