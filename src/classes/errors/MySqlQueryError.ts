import { BaseError } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';

export class MySqlQueryError extends BaseError {
  sql: string;
  values: any[];

  constructor(message: string, values: any[], internal: Error) {
    super(message, internal);
    this.values = values;

    Object.setPrototypeOf(this, MySqlQueryError.prototype);
  }

  static is(error: unknown): error is MySqlQueryError {
    return JS.is.error(this, error);
  }

  static reThrow(sql: string, values: any[]): any {
    return (error: Error) => {
      const e = new MySqlQueryError(error.message, values, error);
      e.sql = sql;
      throw e;
    };
  }

  protected toLogDetails(): object {
    return {
      ...super.toLogDetails(),
      sql: this.sql,
      values: this.values,
    };
  }
}
