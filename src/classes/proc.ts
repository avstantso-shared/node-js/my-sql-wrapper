import type { Arg, Query } from '../types';
import * as Validate from '../validate';
import { Named } from './abstract';

type OnRejected = (reason: any) => PromiseLike<never>;

export class Proc extends Named {
  params: Arg[];
  onrejected: OnRejected;

  constructor(name: string, ...params: Arg[]) {
    params.forEach(Validate.argument.iteration({ allowField: true }));

    super(name);
    this.params = params;

    Object.setPrototypeOf(this, Proc.prototype);
  }

  protected doParse(): Query.Fragment {
    const { sqls, values } = Proc.parseValues(this.params);
    return {
      sql: `CALL ${this.name}(${sqls.join(', ')})`,
      ...(values?.length ? { values } : {}),
    };
  }

  catcher(onrejected?: OnRejected): Proc {
    this.onrejected = onrejected;
    return this;
  }
}
