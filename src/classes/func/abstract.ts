import type { Query, Convert, ArgType } from '../../types';
import * as Validate from '../../validate';
import { NamedConv } from '../abstract';

export namespace Func {
  export type CustomJSON = {};
  export type ReturnType = ArgType | CustomJSON;
}

export class Func<
  TResult extends Func.ReturnType = Func.ReturnType
> extends NamedConv {
  params: any[];

  constructor(name: string, convert: Convert, ...params: any[]) {
    params.forEach(Validate.argument.iteration({ allowField: true }));

    super(name, convert);
    this.params = params;

    Object.setPrototypeOf(this, Func.prototype);
  }

  protected doParse(): Query.Fragment {
    const { sqls, values } = Func.parseValues(this.params);
    return {
      sql: `${this.name}(${sqls.join(', ')})`,
      ...(values?.length ? { values } : {}),
    };
  }
}
