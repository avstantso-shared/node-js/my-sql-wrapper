import type { Convert, Arg } from '../../types';
import { toBool, toDate } from '../../converters';
import * as Abstract from './abstract';

export namespace Func {
  export type ReturnType = Abstract.Func.ReturnType;
}

export class Func<
  TResult extends Func.ReturnType = Func.ReturnType
> extends Abstract.Func {
  constructor(name: string, convert: Convert, ...params: Arg[]) {
    super(name, convert, ...params);

    Object.setPrototypeOf(this, Func.prototype);
  }

  public static raw<TResult extends Func.ReturnType = Func.ReturnType>(
    name: string,
    ...params: Arg[]
  ) {
    return new Func<TResult>(name, null, ...params);
  }

  public static bool(name: string, ...params: Arg[]) {
    return new Func<boolean>(name, toBool, ...params);
  }

  public static date(name: string, ...params: Arg[]) {
    return new Func<Date>(name, toDate, ...params);
  }
}
