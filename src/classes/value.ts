import { JS } from '@avstantso/node-or-browser-js--utils';
import type { ArgType, Query } from '../types';
import { MySqlWrapperArgError } from './errors';
import { Parsable } from './abstract';

export class Value<T extends ArgType = ArgType> extends Parsable {
  value: T;

  constructor(value: T) {
    Value.internalValidate(value);

    super();
    this.value = value;

    Object.setPrototypeOf(this, Value.prototype);
  }

  private static internalValidate<T extends ArgType = ArgType>(value: T) {
    if (
      JS.is.object<any>(value) &&
      !(value instanceof Buffer || value instanceof Date)
    )
      throw MySqlWrapperArgError.for(this, value.constructor.name);
  }

  protected doParse(): Query.Fragment {
    return { sql: `?`, values: [this.value] };
  }

  public static helper<T extends ArgType>(value: T): Value<T> {
    return new Value<T>(value);
  }
}
