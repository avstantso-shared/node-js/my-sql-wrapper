import { Generics, JS } from '@avstantso/node-or-browser-js--utils';
import type { ArgType, Query } from '../../types';
import { Parsable } from '../abstract';
import { Field } from '../field';
import { MySqlWrapperArgError } from '../errors';

export namespace Desc {
  export type Type = Field<ArgType> | string;
}

export class Desc<T extends Desc.Type = Desc.Type> extends Parsable {
  named: T;

  constructor(named: T) {
    Desc.internalValidate<T>(named);

    super();
    this.named = named;

    Object.setPrototypeOf(this, Desc.prototype);
  }

  private static internalValidate<T extends Desc.Type = Desc.Type>(named: T) {
    if (JS.is.string(named)) return;

    if (!JS.is.object(named))
      throw MySqlWrapperArgError.for(this, typeof named);

    if (!(named instanceof Field))
      throw MySqlWrapperArgError.for(
        this,
        Generics.Cast(named).constructor.name
      );
  }

  protected doParse(): Query.Fragment {
    return {
      sql: `${this.named instanceof Field ? this.named.name : this.named} DESC`,
    };
  }

  public static helper<T extends Desc.Type = Desc.Type>(value: T): Desc<T> {
    return new Desc(value);
  }
}
