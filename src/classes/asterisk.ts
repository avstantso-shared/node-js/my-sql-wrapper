import type { ArgType, Query } from '../types';
import { Named } from './abstract';

export class Asterisk<T extends ArgType = ArgType> extends Named {
  constructor() {
    super('*');

    Object.setPrototypeOf(this, Asterisk.prototype);
  }

  protected doParse(): Query.Fragment {
    return { sql: `${this.name}` };
  }
}
