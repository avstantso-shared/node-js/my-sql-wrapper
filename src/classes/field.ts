import { JS } from '@avstantso/node-or-browser-js--utils';
import type { Query, Convert, ArgType } from '../types';
import { NamedConv } from './abstract';

export class Field<T extends ArgType = ArgType> extends NamedConv {
  size: number;

  constructor(
    sizeOrConvert1?: number | Convert,
    sizeOrConvert2?: number | Convert
  ) {
    let size: number;
    let convert: Convert;

    [sizeOrConvert1, sizeOrConvert2].forEach((soc) => {
      if (JS.is.number(soc)) size = soc as number;
      else if (JS.is.function(soc)) convert = soc as Convert;
    });

    super(undefined, convert);
    this.size = size;

    Object.setPrototypeOf(this, Field.prototype);
  }

  protected doParse(): Query.Fragment {
    return { sql: `${'`'}${this.name}${'`'}` };
  }

  public static named<T extends ArgType = ArgType>(
    name: string,
    sizeOrConvert1?: number | Convert,
    sizeOrConvert2?: number | Convert
  ) {
    const field = new Field<T>(sizeOrConvert1, sizeOrConvert2);
    field.name = name;
    return field;
  }
}
