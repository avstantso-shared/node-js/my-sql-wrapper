import { JS } from '@avstantso/node-or-browser-js--utils';
import type { Query, Convert, ArgType } from '../types';
import { MySqlWrapperArgError } from './errors';
import { NamedConv } from './abstract';
import { Func } from './func';
import { Field } from './field';
import { Value } from './value';

export namespace As {
  export type Type = ArgType | Func<ArgType> | Field<ArgType> | Value<ArgType>;
}

export class As<T extends As.Type = As.Type> extends NamedConv {
  value: T;

  constructor(value: T, name: string) {
    As.internalValidate(value);

    const convert: Convert =
      value instanceof NamedConv ? value.convert : undefined;

    super(name, convert);

    this.value = value;

    Object.setPrototypeOf(this, As.prototype);
  }

  private static internalValidate<T extends As.Type = As.Type>(value: T) {
    if (
      JS.is.object<any>(value) &&
      !(
        value instanceof Func ||
        value instanceof Field ||
        value instanceof Value ||
        value instanceof Buffer ||
        value instanceof Date
      )
    )
      throw MySqlWrapperArgError.for(this, value.constructor.name);
  }

  protected doParse(): Query.Fragment {
    const { sql, values } = As.parseValue(this.value);
    return {
      sql: `${sql} as ${this.name}`,
      ...(values?.length ? { values } : {}),
    };
  }

  public static helper<T extends As.Type = As.Type>(
    value: T,
    name: string
  ): As<T> {
    return new As<T>(value, name);
  }
}
