import type { Arg } from '../../../types';
import { Comparison } from '../abstract';

export class Neq extends Comparison {
  constructor(...values: [Arg, Arg]) {
    super(...values);
    Object.setPrototypeOf(this, Neq.prototype);
  }

  protected sign(): string {
    return '<>';
  }

  public static get helper() {
    return Comparison.makeHelper(this);
  }
}
