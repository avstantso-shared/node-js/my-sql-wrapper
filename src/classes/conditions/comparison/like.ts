import type { Arg } from '../../../types';
import { Comparison } from '../abstract';

export class Like extends Comparison {
  constructor(...values: [Arg, Arg]) {
    super(...values);
    Object.setPrototypeOf(this, Like.prototype);
  }

  protected sign(): string {
    return Like.name.toLocaleUpperCase();
  }

  public static get helper() {
    return Comparison.makeHelper(this);
  }
}
