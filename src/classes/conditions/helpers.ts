import { Eq, Gt, Gte, Like, Lt, Lte, Neq, NullEq } from './comparison';
import { And, Or, Xor } from './condition-list';
import { Not } from './not';
import { IsNull, IsNotNull } from './is--null';

const Comparisons = {
  Eq: Eq.helper,
  Gt: Gt.helper,
  Gte: Gte.helper,
  Like: Like.helper,
  Lt: Lt.helper,
  Lte: Lte.helper,
  Neq: Neq.helper,
  NullEq: NullEq.helper,
};

const ConditionLists = {
  And: And.helper,
  Or: Or.helper,
  Xor: Xor.helper,
};

const IsNulls = {
  IsNotNull: IsNotNull.helper,
  IsNull: IsNull.helper,
};

export const Conditions = {
  ...Comparisons,
  ...ConditionLists,
  ...IsNulls,

  Not: Not.helper,
};

export type Conditions = typeof Conditions;
