import type { Query, Arg } from '../../../types';
import { Condition } from '../abstract';

export class IsNull extends Condition {
  value: Arg;

  constructor(value: Arg) {
    super();
    this.value = value;
    Object.setPrototypeOf(this, IsNull.prototype);
  }

  protected doParse(): Query.Fragment {
    const { sql, values } = IsNull.parseValue(this.value);
    return {
      sql: `${sql} IS NULL`,
      ...(values ? { values } : {}),
    };
  }

  public static get helper() {
    return (value: Arg) => new IsNull(value);
  }
}
