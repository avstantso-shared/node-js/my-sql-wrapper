import type { Query, Arg } from '../../../types';
import { Condition } from '../abstract';

export class IsNotNull extends Condition {
  value: Arg;

  constructor(value: Arg) {
    super();
    this.value = value;
    Object.setPrototypeOf(this, IsNotNull.prototype);
  }

  protected doParse(): Query.Fragment {
    const { sql, values } = IsNotNull.parseValue(this.value);
    return {
      sql: `${sql} IS NOT NULL`,
      ...(values ? { values } : {}),
    };
  }

  public static get helper() {
    return (value: Arg) => new IsNotNull(value);
  }
}
