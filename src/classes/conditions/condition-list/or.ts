import { Condition, ConditionList } from '../abstract';

export class Or extends ConditionList {
  constructor(
    condition1: Condition,
    condition2: Condition,
    ...conditions: Condition[]
  ) {
    super(condition1, condition2, ...conditions);
    Object.setPrototypeOf(this, Or.prototype);
  }

  protected seporator(): string {
    return Or.name.toLocaleUpperCase();
  }

  public static get helper() {
    return ConditionList.makeHelper(this);
  }
}
