import { Condition, ConditionList } from '../abstract';

export class Xor extends ConditionList {
  constructor(
    condition1: Condition,
    condition2: Condition,
    ...conditions: Condition[]
  ) {
    super(condition1, condition2, ...conditions);
    Object.setPrototypeOf(this, Xor.prototype);
  }

  protected seporator(): string {
    return 'XOR';
  }

  public static get helper() {
    return ConditionList.makeHelper(this);
  }
}
