import { Condition, ConditionList } from '../abstract';

export class And extends ConditionList {
  constructor(...conditions: [Condition, Condition, ...Condition[]]) {
    super(...conditions);
    Object.setPrototypeOf(this, And.prototype);
  }

  protected seporator(): string {
    return And.name.toLocaleUpperCase();
  }

  public static get helper() {
    return ConditionList.makeHelper(this);
  }
}
