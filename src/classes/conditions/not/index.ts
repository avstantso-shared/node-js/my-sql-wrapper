import type { Query } from '../../../types';
import { Condition } from '../abstract';

export class Not extends Condition {
  condition: Condition;

  constructor(condition: Condition) {
    Not.internalValidate(condition);

    super();
    this.condition = condition;
    Object.setPrototypeOf(this, Not.prototype);
  }

  private static internalValidate(condition: Condition) {
    Condition.validate(this, condition);
  }

  protected doParse(): Query.Fragment {
    const { sql, values } = this.condition.parse();
    return {
      sql: `NOT (${sql})`,
      ...(values ? { values } : {}),
    };
  }

  public static get helper() {
    return (condition: Condition) => new Not(condition);
  }
}
