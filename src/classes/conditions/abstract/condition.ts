import { Generics, JS } from '@avstantso/node-or-browser-js--utils';

import { Parsable } from '../../abstract';
import { MySqlWrapperArgError } from '../../errors';

export class Condition extends Parsable {
  constructor() {
    super();
    Object.setPrototypeOf(this, Condition.prototype);
  }

  protected static validate<T extends object>(
    objClass: new (...args: any) => T,
    condition: Condition,
    index?: number
  ) {
    if (!JS.is.object(condition))
      throw MySqlWrapperArgError.for(objClass, typeof condition, index);

    if (!(condition instanceof Condition))
      throw MySqlWrapperArgError.for(
        objClass,
        Generics.Cast(condition).constructor.name,
        index
      );
  }
}
