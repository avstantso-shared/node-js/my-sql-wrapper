import { AbstractError } from '@avstantso/node-or-browser-js--errors';
import { Query } from '../../../types';
import { Condition } from './condition';

export class ConditionList extends Condition {
  conditions: Condition[];

  constructor(...conditions: [Condition, Condition, ...Condition[]]) {
    ConditionList.internalValidate(conditions);

    super();
    this.conditions = conditions;
    Object.setPrototypeOf(this, ConditionList.prototype);
  }

  private static internalValidate(list: Condition[]) {
    list.forEach((item, index) => Condition.validate(this, item, index));
  }

  protected seporator(): string {
    throw new AbstractError();
  }

  protected doParse(): Query.Fragment {
    const sqls: string[] = [];
    const values: any[] = [];

    this.conditions.forEach((c) => {
      const p = c.parse();
      sqls.push(p.sql);
      values.push(...(p.values || []));
    });

    return {
      sql: `(${sqls.join(`) ${this.seporator()} (`)})`,
      ...(values.length ? { values } : {}),
    };
  }

  protected static makeHelper<T extends ConditionList>(
    objClass: new (...conditions: [Condition, Condition, ...Condition[]]) => T
  ) {
    return (...conditions: [Condition, Condition, ...Condition[]]) =>
      new objClass(...conditions);
  }
}
