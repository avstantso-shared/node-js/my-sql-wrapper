import { AbstractError } from '@avstantso/node-or-browser-js--errors';
import type { Arg, Query } from '../../../types';
import { Condition } from './condition';

export class Comparison extends Condition {
  values: Arg[];

  constructor(...values: [Arg, Arg]) {
    super();
    this.values = values;
    Object.setPrototypeOf(this, Comparison.prototype);
  }

  protected sign(): string {
    throw new AbstractError();
  }

  protected doParse(): Query.Fragment {
    const v1 = Comparison.parseValue(this.values[0]);
    const v2 = Comparison.parseValue(this.values[1]);
    const values = [...(v1.values || []), ...(v2.values || [])];

    return {
      sql: `${v1.sql} ${this.sign()} ${v2.sql}`,
      ...(values.length ? { values } : {}),
    };
  }

  protected static makeHelper<T extends Comparison>(
    objClass: new (...values: [Arg, Arg]) => T
  ) {
    return (...values: [Arg, Arg]) => new objClass(...values);
  }
}
