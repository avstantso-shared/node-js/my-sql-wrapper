import { ArgType } from '../types';
import { Parsable, Prefix } from './abstract';

export class Distinct<T extends ArgType = ArgType> extends Prefix {
  constructor(content: Parsable) {
    super(Distinct.prefix, content);
    Object.setPrototypeOf(this, Distinct.prototype);
  }

  static prefix = Distinct.name.toLocaleUpperCase();

  public static helper<T extends ArgType = ArgType>(
    content: Parsable
  ): Distinct<T> {
    return new Distinct(content);
  }
}
