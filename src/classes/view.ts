import type { ViewFields } from '../types/viewFields';

export class View<TFields extends ViewFields> {
  name: string;
  fields: TFields;

  constructor(name: string, fields: TFields) {
    this.name = name;
    Object.keys(fields).forEach((name) => (fields[name].name = name));
    this.fields = fields;

    Object.setPrototypeOf(this, View.prototype);
  }
}
