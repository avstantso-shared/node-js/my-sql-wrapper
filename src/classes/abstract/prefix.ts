import type { Query } from '../../types';
import { Parsable } from './parsable';

export class Prefix extends Parsable {
  prefix: string;
  content: Parsable;

  constructor(prefix: string, content: Parsable) {
    super();
    this.prefix = prefix;
    this.content = content;

    Object.setPrototypeOf(this, Prefix.prototype);
  }

  protected doParse(): Query.Fragment {
    const { sql, values } = this.content.parse();
    return { sql: `${this.prefix} ${sql}`, values };
  }
}
