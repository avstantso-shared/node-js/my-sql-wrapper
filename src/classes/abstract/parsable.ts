import { JS } from '@avstantso/node-or-browser-js--utils';
import { AbstractError } from '@avstantso/node-or-browser-js--errors';
import { Query } from '../../types';

export class Parsable {
  constructor() {
    Object.setPrototypeOf(this, Parsable.prototype);
  }

  public static parseValue(value: any): Query.Fragment {
    if (value instanceof Parsable) {
      const { sql, values } = (value as Parsable).parse();
      return {
        sql,
        ...(values?.length ? { values } : {}),
      };
    }

    if (value instanceof Buffer || value instanceof Date)
      return { sql: `?`, values: [value] };

    if (JS.is.string(value)) return { sql: `'${value}'` };

    if (undefined === value || null === value) return { sql: `null` };

    return { sql: `${value}` };
  }

  public static parseValues(list: any[]): Query.Fragment.List {
    const sqls: string[] = [];
    let values: any[];

    list.forEach((value) => {
      const v = Parsable.parseValue(value);
      sqls.push(v.sql);
      if (v.values) !values ? (values = v.values) : values.push(...v.values);
    });

    return {
      sqls,
      ...(values?.length ? { values } : {}),
    };
  }

  protected doParse(): Query.Fragment {
    throw new AbstractError();
  }

  public parse(): Query.Fragment {
    return this.doParse();
  }
}
