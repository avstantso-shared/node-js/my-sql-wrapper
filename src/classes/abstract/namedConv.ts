import type { Convert } from '../../types/convertable';
import { Named } from './named';

export class NamedConv extends Named {
  convert: Convert;

  constructor(name: string, convert: Convert) {
    super(name);
    this.convert = convert;

    Object.setPrototypeOf(this, NamedConv.prototype);
  }
}
