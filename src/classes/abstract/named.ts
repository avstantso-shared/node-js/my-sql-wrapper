import { Parsable } from './parsable';

export class Named extends Parsable {
  name: string;
  constructor(name: string) {
    super();
    this.name = name;

    Object.setPrototypeOf(this, Named.prototype);
  }
}
