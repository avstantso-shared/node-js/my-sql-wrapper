import { Generics, JS } from '@avstantso/node-or-browser-js--utils';
import type { Arg, ArgType } from '../types';
import { MySqlWrapperArgError } from '../classes/errors';
import { Func } from '../classes/func';
import { Value } from '../classes/value';
import { Field } from '../classes/field';
import { As } from '../classes/as';

const simple = [JS.number, JS.string, JS.boolean, JS.undefined];

export namespace Argument {
  export namespace Options {
    export type Base = { allowField?: boolean };
  }

  export type Options = Options.Base & { index?: number };
}

export function argument<T extends ArgType = ArgType>(
  arg: Arg<T>,
  options?: Argument.Options
) {
  const { allowField, index } = options || {};

  const t = typeof arg;
  if (simple.includes(Generics.Cast.To(t))) return;

  let nested: string;
  if (JS.object !== t) nested = t;
  else if (
    undefined === arg ||
    null === arg ||
    (arg instanceof Field && allowField) ||
    arg instanceof Func ||
    arg instanceof Value ||
    arg instanceof As ||
    arg instanceof Buffer ||
    arg instanceof Date
  )
    return;
  else nested = arg.constructor.name;

  throw new MySqlWrapperArgError('Arg', nested, index);
}

argument.iteration =
  (options?: Argument.Options.Base) => (arg: Arg, index: number) =>
    argument(arg, { ...options, index });
