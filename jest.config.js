const testsConfig = require('./tests/config');

module.exports = async () => {
  await testsConfig();

  return {
    preset: 'ts-jest',
    testEnvironment: 'node',
  };
};
