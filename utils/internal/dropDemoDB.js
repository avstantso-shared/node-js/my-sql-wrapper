const execSql = require('./execSql');

const sql = `DROP DATABASE ${process.env.DB_NAME || 'avstantso_demo'}`;

const dropDemoDB = async (options = { silent: false }) => {
  const { silent } = options;
  await execSql({}, async (conn) => conn.query(sql));
  if (!silent) {
    console.log('\x1b[32mSuccess!\x1b[0m');
  }
};

module.exports = dropDemoDB;
