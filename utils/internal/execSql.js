const util = require('util');
const mysql = require('mysql2/promise');
const Promise = require('bluebird');

const unquote = (s = '') => s.replace(/(^['"]*)|(['"]*$)/g, '');

const execSql = async (config, doExec) => {
  const conf = {
    Promise,
    host: unquote(process.env.DB_HOST) || 'localhost',
    port: Number.parseInt(unquote(process.env.DB_PORT), 10) || 3306,
    user: unquote(process.env.DB_USER) || 'root',
    password: unquote(process.env.DB_PASSWORD),
    ...config,
  };

  try {
    const mysqlConn = await mysql.createConnection(conf);
    try {
      await doExec(mysqlConn);
    } finally {
      await mysqlConn.end();
    }
  } catch (e) {
    console.log(
      `Config: ${util.inspect(conf, {
        depth: Infinity,
        maxArrayLength: Infinity,
      })}`
    );
    throw e;
  }
};

module.exports = execSql;
