CREATE DATABASE IF NOT EXISTS `avstantso_demo`;
USE `avstantso_demo`;

SET NAMES utf8 ;

DROP TABLE IF EXISTS `spells`;
DROP TABLE IF EXISTS `magics`;
DROP VIEW IF EXISTS `vw_spells`;

SET character_set_client = utf8mb4 ;
CREATE TABLE `magics` (
  `id` binary(16) NOT NULL,
  `putdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `upddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- split!
CREATE TRIGGER `magic_BEFORE_INSERT` BEFORE INSERT ON `magics` FOR EACH ROW BEGIN
	IF NEW.id iS NULL THEN
		SET NEW.id = UUID_TO_BIN(UUID(), 1);
    END IF;
END
-- split!
CREATE TRIGGER `magic_BEFORE_UPDATE` BEFORE UPDATE ON `magics` FOR EACH ROW BEGIN
	SET NEW.upddate = CURRENT_TIMESTAMP;
END
-- split!
INSERT INTO `magics`
(`name`, `description`) 
VALUES 
('light','The light magic'),
('dark','The dark magic');


SET character_set_client = utf8mb4 ;
CREATE TABLE `spells` (
  `id` binary(16) NOT NULL,
  `putdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `upddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `magic_id` binary(16) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_spell_magic_idx` (`magic_id`),
  CONSTRAINT `fk_spell_magic` FOREIGN KEY (`magic_id`) REFERENCES `magics` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- split!
CREATE TRIGGER `spells_BEFORE_INSERT` BEFORE INSERT ON `spells` FOR EACH ROW BEGIN
	IF NEW.id iS NULL THEN
		SET NEW.id = UUID_TO_BIN(UUID(), 1);
    END IF;
END
-- split!
CREATE TRIGGER `spells_BEFORE_UPDATE` BEFORE UPDATE ON `spells` FOR EACH ROW BEGIN
	SET NEW.upddate = CURRENT_TIMESTAMP;
END
-- split!
INSERT INTO `spells` 
(`magic_id`, `name`, `description`) 
VALUES 
( (SELECT `id` FROM `magics` WHERE `name` = 'light'),'sun_ray','The ray of sun'),
( (SELECT `id` FROM `magics` WHERE `name` = 'dark'),'curse','The evil curse');

SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
CREATE VIEW `vw_spells` AS select `s`.`id` AS `id`,`s`.`putdate` AS `putdate`,`s`.`upddate` AS `upddate`,`s`.`active` AS `active`,`s`.`name` AS `name`,`s`.`description` AS `description`,`m`.`id` AS `magic_id`,`m`.`name` AS `magic_name` from (`spells` `s` join `magics` `m` on((`s`.`magic_id` = `m`.`id`)));
SET character_set_client = @saved_cs_client;

-- split!
DROP FUNCTION IF EXISTS `IsDarkSpell`;
CREATE FUNCTION `IsDarkSpell`(a_spell_name VARCHAR(30) ) RETURNS tinyint(1)
    READS SQL DATA
BEGIN
  DECLARE m_id BINARY(16) DEFAULT (SELECT `magic_id` FROM `spells` WHERE `name` = a_spell_name);
	RETURN ('dark' = (SELECT `name` FROM `magics` WHERE `id` = m_id));
END
-- split!
DROP FUNCTION IF EXISTS `IsLightSpell`;
CREATE FUNCTION `IsLightSpell`(a_spell_name VARCHAR(30) ) RETURNS tinyint(1)
    READS SQL DATA
BEGIN
  DECLARE m_id BINARY(16) DEFAULT (SELECT `magic_id` FROM `spells` WHERE `name` = a_spell_name);
	RETURN ('light' = (SELECT `name` FROM `magics` WHERE `id` = m_id));
END
-- split!
DROP FUNCTION IF EXISTS `SpellById`;
CREATE FUNCTION `SpellById`(`a_id` BINARY(16) ) RETURNS varchar(30) CHARSET utf8 COLLATE utf8_unicode_ci
    READS SQL DATA
BEGIN
	RETURN (SELECT `name` FROM `spells` WHERE `id` = `a_id`);
END
-- split!
DROP FUNCTION IF EXISTS `SpellId`;
CREATE FUNCTION `SpellId`(`a_name` varchar(30) ) RETURNS BINARY(16)
    READS SQL DATA
BEGIN
	RETURN (SELECT `id` FROM `spells` WHERE `name` = `a_name`);
END
-- split!
DROP PROCEDURE IF EXISTS `SignalIf`;
CREATE PROCEDURE `SignalIf`(a_message_name varchar(499), a_condition BOOL)
BEGIN
	IF (a_condition) THEN
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = a_message_name;
	END IF;
END
-- split!
DROP PROCEDURE IF EXISTS `SpellSelect`;
CREATE PROCEDURE `SpellSelect`(`a_id` BINARY(16))
    READS SQL DATA
BEGIN
	SELECT 
        `s`.`id` AS `id`,
        `s`.`putdate` AS `putdate`,
        `s`.`upddate` AS `upddate`,
        `s`.`active` AS `active`,
        `s`.`name` AS `name`,
        `s`.`description` AS `description`,
        `m`.`id` AS `magic_id`,
        `m`.`name` AS `magic_name`,
        `IsDarkSpell`(`s`.`name`) as `isDark`,
        `IsLightSpell`(`s`.`name`) as `isLight`
    FROM
        (`avstantso_demo`.`spells` `s`
        JOIN `avstantso_demo`.`magics` `m` ON ((`s`.`magic_id` = `m`.`id`)))
	WHERE `s`.`id` = `a_id`;
END
