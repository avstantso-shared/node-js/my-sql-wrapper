const fs = require('fs');
const Promise = require('bluebird');

const execSql = require('./execSql');

const sql = `${fs.readFileSync(`${__dirname}/sql/avstantso_demo.sql`)}`;

const createDemoDB = async (options = { silent: false }) => {
  const { silent } = options;
  return execSql({ multipleStatements: true }, async (conn) => {
    await Promise.mapSeries(sql.split('-- split!'), async (s) => conn.query(s));

    if (!silent) {
      await conn
        .query('SELECT COUNT(*) as `c` FROM `spells`')
        .then(([rows]) => (rows || [{ c: 0 }])[0].c)
        .then((c) =>
          2 == c
            ? console.log('\x1b[32mSuccess!\x1b[0m')
            : console.error('Bad rows count!')
        );
    }
  });
};

module.exports = createDemoDB;
