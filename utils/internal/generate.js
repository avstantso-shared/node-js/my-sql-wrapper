const path = require('path');
const fs = require('fs');
const rimraf = require('rimraf');

const {
  CodeGen: CodeGenG,
} = require('@avstantso/node-js--my-sql-refman-grabber');

const {
  CodeGen: GodeGenD,
} = require('@avstantso/node-js--my-sql-sync-db-code');

const execSql = require('./execSql');

const scriptName = 'my-sql-wrapper-test generate';

const generate = async (options = { silent: false }) => {
  const { silent } = options;
  const destPath = path.resolve(__dirname, '../../tests/generated');

  rimraf.sync(destPath);
  fs.mkdirSync(destPath);

  const { generate: generateG } = CodeGenG({
    pages: [
      'string-functions',
      'miscellaneous-functions',
      'date-and-time-functions',
      'mathematical-functions',
    ],
    silent,
    cachePath: path.resolve(__dirname, '../../tests/grabber-cache'),
  });

  const { defaultOptions: defaultOptionsD, generate: generateD } = GodeGenD({
    silent,
  });

  await generateG(path.resolve(destPath, 'sysFunctions'), scriptName);

  const database = process.env.DB_NAME || 'avstantso_demo';
  await execSql({ database }, async (conn) => {
    return await generateD(conn, database, destPath, scriptName, {
      ...defaultOptionsD,
      schemaEnv: 'DB_NAME',
    });
  });
};

module.exports = generate;
