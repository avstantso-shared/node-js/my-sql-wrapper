const path = require('path');
const dotenv = require('dotenv');
dotenv.config({ path: path.resolve(__dirname, '../.env') });

const createDemoDB = require('./internal/createDemoDB');
createDemoDB();
