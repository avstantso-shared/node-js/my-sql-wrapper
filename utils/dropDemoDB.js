const path = require('path');
const dotenv = require('dotenv');
dotenv.config({ path: path.resolve(__dirname, '../.env') });

const dropDemoDB = require('./internal/dropDemoDB');
dropDemoDB();
