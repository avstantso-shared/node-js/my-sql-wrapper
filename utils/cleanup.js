const path = require('path');
const rimraf = require('rimraf');

const destPath = path.resolve(__dirname, '../tests/generated');
rimraf.sync(destPath);
console.log('\x1b[32mSuccess!\x1b[0m');
